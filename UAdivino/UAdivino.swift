//
//  UAdivino.swift
//  UAdivino
//
//  Created by Master Móviles on 3/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation

class UAdivino {
    
    var nombre : String
    let respuestas = [
        Respuesta(texto: "Si", tipo: true),
        Respuesta(texto: "No", tipo: false),
        Respuesta(texto: "Ni de coña", tipo: false),
        Respuesta(texto: "Por supuesto!", tipo: true)
    ]
    
    struct Respuesta{
        var texto: String
        var tipo: Bool
    }
    
    init(nombre:String) {
        self.nombre = nombre
    }
    
    func obtenerRespuesta() -> Respuesta {
        let indice = Int(arc4random_uniform(UInt32(respuestas.count)));
        return respuestas[indice];
    }
}
