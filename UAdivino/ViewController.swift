//
//  ViewController.swift
//  UAdivino
//
//  Created by Master Móviles on 3/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var respuestaLabel: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var saludoLabel: UILabel!
    
    var miAdivino = UAdivino(nombre: "Rappel")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.saludoLabel.text = "Hola, soy \(miAdivino.nombre)"
    }

    @IBAction func botonPulsado(_ sender: AnyObject) {
        //let respuesta = obtenerRespuesta()
        //self.respuestaLabel.text = respuesta
        let respuesta = self.miAdivino.obtenerRespuesta()
        self.respuestaLabel.text = respuesta.texto
        
        if(respuesta.tipo == true){
            self.imagen.image = UIImage(named:"si")
        }else if(respuesta.tipo == false){
            self.imagen.image = UIImage(named:"angry-no")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}

